# dnf_exporter

Prometheus exporter for pending DNF updates and advisories.

## Usage

This is a simple Python script that is meant to be used as part of the
Prometheus node_exporter [textfile
collector](https://github.com/prometheus/node_exporter#textfile-collector),
where this script is run periodically (e.g., from a systemd timer) and it writes
a text file with prometheus-compatible metrics.

There is no configuration, it writes to the file
`/var/lib/prometheus/node-exporter/dnf.prom`. This is a directory which
the [Fedora
package](https://src.fedoraproject.org/rpms/golang-github-prometheus-node-exporter)
by default is configured to search.

A systemd service and timer are included.

No dependencies outside of the Python standard library are used, it just scrapes
the output of DNF.

## Installation and running

- Copy the script to `/usr/local/bin/`
- Copy the systemd service and timer to `/etc/systemd/systemd/`
- Run `systemctl enable --now dnf_exporter.timer`

## Sample output

For a Fedora installation with some pending updates:

```
# HELP dnf_advisories_pending DNF advisories by type
# TYPE dnf_advisories_pending gauge
dnf_advisories_pending{type="bugfix"} 11
dnf_advisories_pending{type="enhancement"} 3
dnf_advisories_pending{type="newpackage"} 0
dnf_advisories_pending{type="security_critical"} 0
dnf_advisories_pending{type="security_important"} 0
dnf_advisories_pending{type="security_low"} 0
dnf_advisories_pending{type="security_moderate"} 4
dnf_advisories_pending{type="security_unknown"} 0
dnf_advisories_pending{type="unknown"} 3
# HELP dnf_upgrades_pending DNF available updates by repo
# TYPE dnf_upgrades_pending gauge
dnf_upgrades_pending{repo="fedora"} 0
dnf_upgrades_pending{repo="fedora-cisco-openh264"} 0
dnf_upgrades_pending{repo="fedora-modular"} 0
dnf_upgrades_pending{repo="rpmfusion-free"} 0
dnf_upgrades_pending{repo="rpmfusion-free-updates"} 0
dnf_upgrades_pending{repo="rpmfusion-nonfree"} 0
dnf_upgrades_pending{repo="rpmfusion-nonfree-updates"} 0
dnf_upgrades_pending{repo="updates"} 21
dnf_upgrades_pending{repo="updates-modular"} 0
```

## License

[MIT License](LICENSE)
