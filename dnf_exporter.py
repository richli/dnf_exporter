#!/usr/bin/env python3
"""Query for available DNF updates for node_exporter.

This is meant to be used as part of the Prometheus node_exporter [textfile
collector](https://github.com/prometheus/node_exporter#textfile-collector),
where this script is run periodically (e.g., from a systemd timer) and it writes
a text file with prometheus-compatible metrics.

This script justs shells out to `dnf` and parses its output. It would be better
to use the Python API for DNF but it appears to be unstable and not
well-documented. So...this isn't as efficient but it's good enough to get the
job done. Note that it's very brittle since if the DNF output changes, it would
break this script.

This is inspired by a shell script:
https://github.com/prometheus-community/node-exporter-textfile-collector-scripts/blob/master/yum.sh
"""

import os
import subprocess
from collections import Counter
from pathlib import Path


def _parse_adv_type(entry: str) -> str:
    """Parse the advisory type string.

    DNF mangles the security type so it's oddly inconsistent with the rest.
    """
    # See here for reference:
    # https://github.com/rpm-software-management/dnf/blob/master/dnf/cli/commands/updateinfo.py
    if entry in ("bugfix", "unknown", "enhancement", "newpackage"):
        return entry
    elif entry.endswith("/Sec."):
        severity = entry.split("/")[0].lower()
        return f"security_{severity}"
    else:
        print(f"WARNING: unkown advisory type: {entry}")
        return "unknown"


class DnfMetric:
    """The Prometheus metrics for the DNF updates and advisories."""

    def __init__(self) -> None:
        """Initialize the metrics."""
        # Query all the enabled repos
        p = subprocess.run(
            ["/usr/bin/dnf", "repolist", "--enabled"],
            capture_output=True,
            check=True,
            text=True,
        )
        # Skip the first line and parse the first column as the repo id
        lines = p.stdout.splitlines()[1:]
        self.updates = Counter({line.split()[0]: 0 for line in lines})

        self.advisories = Counter(
            {
                "bugfix": 0,
                "unknown": 0,
                "enhancement": 0,
                "newpackage": 0,
                "security_critical": 0,
                "security_important": 0,
                "security_moderate": 0,
                "security_low": 0,
                "security_unknown": 0,
            }
        )

    def query(self) -> None:
        """Query DNF for the available updates."""
        # Clear out all counters
        for repo in self.updates:
            self.updates[repo] = 0
        for type in self.advisories:
            self.advisories[type] = 0

        p = subprocess.run(
            ["/usr/bin/dnf", "--cacheonly", "--quiet", "check-update"],
            capture_output=True,
            text=True,
        )
        # A return code of 100 is returned if there are updates available. But 1
        # is returned for an error.
        if p.returncode == 1:
            p.check_returncode()

        # Parse the third column as the repo id
        lines = p.stdout.splitlines()[1:]
        updated_repos = []
        for line in lines:
            if line.startswith("Obsoleting Packages"):
                # This section is at the end, so skip the rest of the lines
                break
            repo_id = line.split()[2]
            updated_repos.append(repo_id)
        self.updates.update(updated_repos)

        p = subprocess.run(
            ["/usr/bin/dnf", "--cacheonly", "--quiet", "updateinfo", "--list"],
            capture_output=True,
            check=True,
            text=True,
        )

        # Parse the second column as the advisory type. A type of "security" is
        # split into sub-types for the severity.
        advisory_types = []
        for line in p.stdout.splitlines():
            advisory_types.append(_parse_adv_type(line.split()[1]))
        self.advisories.update(advisory_types)

    def __str__(self) -> str:
        """Display the current state of the metrics."""
        lines = [
            "# HELP dnf_upgrades_pending DNF available updates by repo",
            "# TYPE dnf_upgrades_pending gauge",
        ]
        lines.extend(
            f'dnf_upgrades_pending{{repo="{repo}"}} {updates}'
            for repo, updates in self.updates.items()
        )

        lines.extend(
            [
                "# HELP dnf_advisories_pending DNF advisories by type",
                "# TYPE dnf_advisories_pending gauge",
            ]
        )
        lines.extend(
            f'dnf_advisories_pending{{type="{type}"}} {updates}'
            for type, updates in self.advisories.items()
        )

        return "\n".join(lines)

    def write_file(self, out_file: Path) -> None:
        """Write the current state of the metrics to the output file."""
        with out_file.open("wt") as f:
            f.write(str(self) + "\n")


if __name__ == "__main__":
    out_file = Path("/var/lib/prometheus/node-exporter/dnf.prom")
    debug_mode = os.environ.get("DNF_EXPORTER_DEBUG", "0") == "1"

    dnf_metric = DnfMetric()
    dnf_metric.query()
    if debug_mode:
        print(dnf_metric)
    dnf_metric.write_file(out_file)
